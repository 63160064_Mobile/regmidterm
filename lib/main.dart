import 'dart:ui';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:reg/stuInfo.dart';
import 'package:reg/study.dart';

import 'login.dart';

void main() => runApp(const MyApp());

String currentPage = "LoginPage";

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        title: 'REG',
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        scrollBehavior: CustomScrollBehavior(),
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        theme: ThemeData(
          textTheme: const TextTheme(
            bodyText2: TextStyle(fontSize: 18.0),
          ),
          scaffoldBackgroundColor: Colors.lightBlue.shade50,
          appBarTheme: const AppBarTheme(
            backgroundColor: Colors.deepPurple,
          ),
          drawerTheme: DrawerThemeData(
            backgroundColor: Colors.lightBlue.shade50,
          ),
        ),
        home: LoginPage(),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('หน้าหลัก')),
      drawer: const NavigationDrawer(),
      body: SafeArea(
        left: false,
        right: false,
        child: ListView(
          children: <Widget>[
            Container(
              color: Colors.grey,
              child: const Text(
                'ประกาศ',
                style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 30.0),
            Image.network(
              'https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png',
              height: 150,
              width: 150,
            ),
            const SizedBox(height: 30.0),
            const Text(
                '1.แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี(ด่วนที่สุด)'),
            const SizedBox(height: 20.0),
            const Text(
                'ขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิตต่อการให้บริการของสำนักงานอธิการบดี ที่ https://bit.ly/3cyvuuf'),
            const SizedBox(height: 50.0),
            const Text('2.การทำบัตรนิสิตกับธนาคารกรุงไทย'),
            const SizedBox(height: 20.0),
            const Text(
                'กรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท สำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา ส่วนนิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง'),
          ],
        ),
      ),
    );
  }
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const SizedBox(
            height: 115.0,
            child: DrawerHeader(
              decoration: BoxDecoration(color: Colors.deepPurple),
              child: Text(
                'เมนู',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          ListTile(
            title: const Text('หน้าหลัก'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "MainPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const MainPage(),
                ));
                currentPage = "MainPage";
              }
            },
          ),
          ListTile(
            title: const Text('ตารางเรียน/สอบ'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "StudyTablePage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const StudyTablePage(),
                ));
                currentPage = "StudyTablePage";
              }
            },
          ),
          ListTile(
            title: const Text('ประวัตินิสิต'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "StudentInfoPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const StudentInfoPage(),
                ));
                currentPage = "StudentInfoPage";
              }
            },
          ),
          ListTile(
            title: const Text('ออกจากระบบ'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginPage(),
              ));
              currentPage = "LoginPage";
            },
          ),
        ],
      ),
    );
  }
}

class CustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}
