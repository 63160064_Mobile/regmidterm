import 'package:flutter/material.dart';
import 'package:reg/main.dart';

class LoginPage extends StatelessWidget {
  final TextEditingController _username = TextEditingController();
  final TextEditingController _password = TextEditingController();

  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(title: const Text('Login')),
      body: SafeArea(
        child: ListView(
          children: [
            Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 300.0),
                child: Column(
                  children: [
                    const SizedBox(height: 50.0),
                    Image.network(
                      'https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png',
                      height: 150,
                      width: 150,
                    ),
                    const SizedBox(height: 50.0),
                    TextField(
                        controller: _username,
                        decoration:
                            const InputDecoration(labelText: 'Username')),
                    TextField(
                        controller: _password,
                        obscureText: true,
                        decoration:
                            const InputDecoration(labelText: 'Password')),
                    const SizedBox(height: 30.0),
                    ElevatedButton(
                        onPressed: () {
                          if (_username.text.isEmpty ||
                              _password.text.isEmpty) {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: const Text("Error"),
                                    // content: Text("กรุณากรอกข้อมูลให้ครบถ้วน"),
                                    content: _username.text.isEmpty &&
                                            _password.text.isEmpty
                                        ? const Text(
                                            "กรุณากรอก username และ password")
                                        : _username.text.isEmpty
                                            ? const Text("กรุณากรอก username")
                                            : const Text("กรุณากรอก password"),
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text("ตกลง")),
                                    ],
                                  );
                                });
                          } else {
                            Navigator.of(context)
                                .pushReplacement(MaterialPageRoute(
                              builder: (context) => const MainPage(),
                            ));
                            currentPage = "MainPage";
                          }
                        },
                        child: const Text('Login')),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
